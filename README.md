# Bax Music - Rick and Morty Case

[Github repository](https://github.com/Baxshopnl/Rick-and-Morty-Case)

## Features

* **Dockerized** with Nginx and PHP 7.4 FPM.
* **Symfony 5** with **Webpack Encore Bundle** for asset management.
* **SCSS**, **Typescript** and **Handlebars** for the UI.

## Installation

Pretty straightforward.
1. Using your favorite terminal, navigate to your Projects folder (*this could be anywhere e.g /foo/Documents/Projects*) then clone this repository.
```
git clone https://gitlab.com/jnmrclmbsjse/bax-rick-and-morty.git
```
> Alternatively, you can download the source code.
2. Inside the cloned repository folder, install the php and javascript dependencies then build the assets.
```
composer install
yarn install
yarn build
```
3. Build and run the docker containers using docker-compose.
```
docker-compose up -d
```
4. Open your browser and go to [http://localhost:6001/](http://localhost:6001)
> Note that **6001** is the port that the nginx uses on the host. If 6001 has conflicts on your running services. Please do change it on the **docker-compose.yml**.

5. That's it, you have the project runing on your local!

## Author
* **Junmar Jose** - [Gitlab](https://gitlab.com/jnmrclmbsjse)
