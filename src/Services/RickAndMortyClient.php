<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class RickAndMortyClient {
    public $client;
    private $baseUri = "https://rickandmortyapi.com/api"; // Environment variable can be used here

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function get($endpoint, $params = [], $headers = []) {
        if (isset($params['page']) && $params['page']) {
            $page = $params['page'];
            $response = $this->client->request(
                'GET',
                "$this->baseUri/$endpoint",
                [
                    'query' => [
                        'page' => $page
                    ]
                ]
            );
        } else if (isset($params['id']) && $params['id']) {
            $id = $params['id'];
            $response = $this->client->request(
                'GET',
                "$this->baseUri/$endpoint/$id",
            );
        } else {
            $response = $this->client->request(
                'GET',
                "$this->baseUri/$endpoint"
            );
        }

        return $response->toArray();
    }

}