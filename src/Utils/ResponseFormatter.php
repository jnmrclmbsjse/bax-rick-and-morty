<?php

namespace App\Utils;

abstract class ResponseFormatter
{
    public $body;
    public $isMultiple;

    public function format()
    {
        $response = [];

        if ($this->isMultiple) {
            $response = $this->multipleParsing();
        } else {
            $response = $this->singleParsing();
        }

        $response['status'] = "Successful";
        $response['code'] = 200;
        $response['error'] = null;

        return $response;
    }

    abstract public function multipleParsing();

    abstract public function singleParsing();
}