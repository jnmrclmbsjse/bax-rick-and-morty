<?php

namespace App\Utils\ResponseFormatter;

use App\Utils\ResponseFormatter;

class CharacterFormatter extends ResponseFormatter
{
    public function multipleParsing() {
        $results = $this->body['results'] ?? $this->body;;
        $response = [];
        foreach ($results as $character) {
            $builder['id'] = $character['id'];
            $builder['name'] = $character['name'];
            $builder['image'] = $character['image'];
            $builder['species'] = $character['species'];
            $builder['created'] = $character['created'];
            $response['characters'][] = $builder;
        }

        return $response;
    }

    public function singleParsing() {
        $character = $this->body;

        $builder = $character;
        $builder['episode'] = $this->parseEpisodes($character['episode']);
        $builder['location'] = $this->parseLocation($character['location']);
        $builder['origin'] = $this->parseLocation($character['origin']);
        $builder['type'] = $character['type'] == '' ? 'Unknown' : $character['type'];
        unset($builder['url']);
        $response = [];

        $response['character'] = $builder;

        return $response;
    }

    private function parseEpisodes($episodes) {
        $result = [];
        foreach ($episodes as $episode) {
            $segments = explode('/', $episode);
            $result[] = [
                'id' => end($segments)
            ];
        }

        return $result;
    }

    private function parseLocation($location) {

        $result = [];
        $segments = explode('/', $location['url']);
        $result['id'] = end($segments);
        $result['name'] = $location['name'];

        return $result;
    }
}