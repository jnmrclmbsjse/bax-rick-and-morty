<?php

namespace App\Utils\ResponseFormatter;

use App\Utils\ResponseFormatter;

class LocationFormatter extends ResponseFormatter
{
    public function multipleParsing() {
        $results = $this->body['results'] ?? $this->body;;
        $response = [];
        foreach ($results as $location) {
            $builder['id'] = $location['id'];
            $builder['name'] = $location['name'];
            $builder['type'] = $location['type'];
            $builder['image'] = $location['dimension'];
            $builder['created'] = $location['created'];
            $response['locations'][] = $builder;
        }

        return $response;
    }

    public function singleParsing() {
        $location = $this->body;

        $builder = $location;
        $builder['characters'] = $this->parseCharacters($location['residents']);
        $builder['type'] = $location['type'] == '' ? 'Unknown' : $location['type'];
        unset($builder['residents']);
        unset($builder['url']);
        $response = [];

        $response['location'] = $builder;

        return $response;
    }

    private function parseCharacters($residents) {
        $result = [];
        foreach ($residents as $resident) {
            $segments = explode('/', $resident);
            $result[] = [
                'id' => end($segments)
            ];
        }

        return $result;
    }
}