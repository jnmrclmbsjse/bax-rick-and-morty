<?php

namespace App\Utils\ResponseFormatter;

use App\Utils\ResponseFormatter;

class EpisodeFormatter extends ResponseFormatter
{
    public function multipleParsing() {
        $results = $this->body['results'] ?? $this->body;
        $response = [];
        foreach ($results as $episode) {
            $builder['id'] = $episode['id'];
            $builder['name'] = $episode['name'];
            $builder['episode'] = $episode['episode'];
            $builder['air_date'] = $episode['air_date'];
            $response['episodes'][] = $builder;
        }

        return $response;
    }

    public function singleParsing() {
        $episode = $this->body;

        $builder = $episode;
        $builder['characters'] = $this->parseCharacters($episode['characters']);
        unset($builder['url']);
        $response = [];

        $response['episode'] = $builder;

        return $response;
    }

    private function parseCharacters($residents) {
        $result = [];
        foreach ($residents as $resident) {
            $segments = explode('/', $resident);
            $result[] = [
                'id' => end($segments)
            ];
        }

        return $result;
    }
}