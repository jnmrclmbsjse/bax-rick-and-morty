<?php

namespace App\Controller\Apis;

use App\Services\RickAndMortyClient;
use App\Utils\ResponseFormatter\CharacterFormatter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
* @Route("/api/character", name="api_characters")
*/
class CharacterController extends AbstractController
{
    /**
     * @Route("s");
     * @Route("s/{page}", name="api_characters_getAll", methods={"GET"});
     */
    public function getCharacters(RickAndMortyClient $client, int $page = null): JsonResponse
    {
        $response = $client->get('character', [
            'page' => $page
        ]);

        $formatter = new CharacterFormatter();
        $formatter->body = $response;
        $formatter->isMultiple = true;

        $characters = $formatter->format();

        return new JsonResponse($characters);
    }

    /**
     * @Route("/{id}", name="api_characters_getOne")
     *
     * Retrieves character collection by given IDs
     * when id parameter has comma
     */
    public function getSingleCharacter(RickAndMortyClient $client, string $id = null): JsonResponse
    {
        $response = $client->get('character', [
            'id' => $id
        ]);

        $formatter = new CharacterFormatter();
        $formatter->body = $response;
        $formatter->isMultiple = strpos($id, ',');
        $character = $formatter->format();

        return new JsonResponse($character);
    }
}
