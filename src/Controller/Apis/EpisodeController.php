<?php

namespace App\Controller\Apis;

use App\Services\RickAndMortyClient;
use App\Utils\ResponseFormatter\EpisodeFormatter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
* @Route("/api/episode", name="api_episodes")
*/
class EpisodeController extends AbstractController
{
    /**
     * @Route("s");
     * @Route("s/{page}", name="api_episodes_getAll", methods={"GET"});
     */
    public function getEpisodes(RickAndMortyClient $client, int $page = null): JsonResponse
    {
        $response = $client->get('episode', [
            'page' => $page
        ]);

        $formatter = new EpisodeFormatter();
        $formatter->body = $response;
        $formatter->isMultiple = true;

        $locations = $formatter->format();

        return new JsonResponse($locations);
    }

    /**
     * @Route("/{id}", name="api_episodes_getOne")
     *
     * Retrieves episode collection by given IDs
     * when id parameter has comma
     */
    public function getSingleEpisode(RickAndMortyClient $client, string $id = null): JsonResponse
    {
        $response = $client->get('episode', [
            'id' => $id
        ]);
        $formatter = new EpisodeFormatter();
        $formatter->body = $response;
        $formatter->isMultiple = strpos($id, ',');
        $episodes = $formatter->format();

        return new JsonResponse($episodes);
    }
}
