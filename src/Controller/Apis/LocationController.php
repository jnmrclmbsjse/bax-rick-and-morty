<?php

namespace App\Controller\Apis;

use App\Services\RickAndMortyClient;
use Symfony\Component\Routing\Annotation\Route;
use App\Utils\ResponseFormatter\LocationFormatter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
* @Route("/api/location", name="api_locations")
*/
class LocationController extends AbstractController
{
    /**
     * @Route("s");
     * @Route("s/{page}", name="api_locations_getAll", methods={"GET"});
     */
    public function getLocations(RickAndMortyClient $client, int $page = 1): JsonResponse
    {
        $response = $client->get('location', [
            'page' => $page
        ]);

        $formatter = new LocationFormatter();
        $formatter->body = $response;
        $formatter->isMultiple = true;

        $locations = $formatter->format();

        return new JsonResponse($locations);
    }

    /**
     * @Route("/{id}", name="api_locations_getOne")
     *
     * Retrieves location collection by given IDs
     * when page parameter has comma
     */
    public function getSingleLocation(RickAndMortyClient $client, string $id = null): JsonResponse
    {
        $response = $client->get('location', [
            'id' => $id
        ]);
        $formatter = new LocationFormatter();
        $formatter->body = $response;
        $formatter->isMultiple = strpos($id, ',');
        $locations = $formatter->format();

        return new JsonResponse($locations);
    }
}
