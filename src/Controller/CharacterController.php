<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CharacterController extends AbstractController
{
    /**
     * @Route("/character/{id}", name="character_index")
     */
    public function index(int $id): Response
    {
        return $this->render('character/index.html.twig', [
            'id' => $id,
        ]);
    }
}
