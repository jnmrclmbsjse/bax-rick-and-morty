<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LocationController extends AbstractController
{
    /**
     * @Route("/location/{id}", name="location_index")
     */
    public function index(int $id): Response
    {
        return $this->render('location/index.html.twig', [
            'id' => $id,
        ]);
    }
}
