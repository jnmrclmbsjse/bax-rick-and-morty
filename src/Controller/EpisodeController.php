<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EpisodeController extends AbstractController
{
    /**
     * @Route("/episode/{id}", name="episode_index")
     */
    public function index(int $id): Response
    {
        return $this->render('episode/index.html.twig', [
            'id' => $id,
        ]);
    }
}
