class PageLoader {
    private loader: HTMLElement | null = null;

    constructor() {
        this.loader = document.getElementById("pageloader");
    }

    open() {
        if (this.loader) {
            this.loader.classList.add('is-active');
        }
    }

    close() {
        if (this.loader) {
            this.loader.classList.remove('is-active');
        }
    }
}

export default PageLoader;
