export type CharacterType = {
    created: string,
    episode: {
        id: number
    }[],
    gender: string,
    id: number,
    image: string,
    location: {
        id: number,
        name: string
    }
    name: string,
    origin: {
        id: string,
        name: string
    },
    species: string,
    status: string,
    type: string
}

export type LocationType = {
    created: string,
    dimension: string,
    id: number,
    residents: {
        id: number
    }[],
    name: string,
    type: string
}

export type EpisodeType = {
    air_date: string,
    characters: {
        id: number
    }[],
    created: string,
    episode: string,
    id: number,
    name: string
}

