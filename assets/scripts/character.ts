import Base from '@scripts/base';
import axios from 'axios';

class Character extends Base {
    load() {
        if (this.subContainer) {
            this.subContainer.innerHTML = "Loading all episodes, please wait...";
        }
        this.loadMain();
        this.pageLoader.open();
    }

    async loadMain() {
        this.pageLoader.open();
        let { data } = await axios.get(`/api/character/${this.id}`);
        this.entity = data.character;
        const template = await this.generateCharacterTemplate();

        if (this.mainContainer !== null) {
            this.mainContainer.innerHTML = template;
        }
        setTimeout(() => {
            this.pageLoader.close();
            this.initializePaginator();
            this.loadSub();
        }, 1000);
    }

    async loadSub() {
        let episodes: string[] = [];
        if (this.entity) {
            Object.values(this.entity.episode).forEach((episode: any) => {
                episodes.push(episode.id);
            });
        }

        let ids = episodes.join(',');
        let { data } = await axios.get(`/api/episode/${ids}`);

        const template = this.generateEpisodeTemplate(true, data);

        if (this.subContainer !== null) {
            this.subContainer.innerHTML = template;
        }
        setTimeout(() => {
            this.pageLoader.close()
        }, 1000);
    }

}

new Character('character', 'episodes', 'characterTextInput').load();
