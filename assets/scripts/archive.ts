import axios from 'axios';
import CharacterTemplate from '@templates/handlebars/characters.handlebars';
import LocationTemplate from '@templates/handlebars/locations.handlebars';
import EpisodeTemplate from '@templates/handlebars/episodes.handlebars';
import { CharacterType, EpisodeType, LocationType } from '@scripts/utils/types';
import PageLoader from "@scripts/utils/pageloader";

class Archive {
    private catalogueContainer: HTMLElement | null;
    private pageLoader: PageLoader;
    private page: number;
    private category: string;

    constructor() {
        this.catalogueContainer = document.getElementById('catalogue');
        this.pageLoader = new PageLoader();
        this.page = 1;
        this.category = "characters";
    }

    load() {
        this.initializeButtons();
        this.loadCatalogue();
        this.pageLoader.open();
    }

    private initializeButtons() {
        const rightPaginator: HTMLElement | null = document.getElementById('paginate-right');
        const leftPaginator: HTMLElement | null = document.getElementById('paginate-left');
        const dropdown: HTMLElement | null = document.getElementById('dropdown-category');
        if (dropdown) {
            const categories = dropdown.getElementsByClassName('dropdown-item');

            for(let index = 0; index < categories.length; index++){
                categories[index].addEventListener('click', (event) => {
                    this.page = 1;
                    this.category = categories[index].getAttribute('rnm-category') || 'characters';
                    this.loadCatalogue();
                });
            }
        }

        if (rightPaginator) {
            rightPaginator.addEventListener('click', () => {
                if (this.page) {
                    this.page++;
                }

                this.loadCatalogue();
            });
        }

        if (leftPaginator) {
            leftPaginator.addEventListener('click', () => {
                if (this.page > 1) {
                    this.page--;
                }

                this.loadCatalogue();
            });
        }
    }

    private async loadCatalogue() {
        this.pageLoader.open();
        let { data } = await axios.get(`/api/${this.category}/${this.page}`);

        switch (this.category) {
            case 'characters':
                this.renderCharacters(data);
                break;
            case 'locations':
                this.renderLocations(data);
                break;
            case 'episodes':
                this.renderEpisodes(data);
                break;
        }

        setTimeout(() => {
            this.pageLoader.close()
        }, 1000);
    }

    private renderCharacters({ characters }: any) {
        let builder = "";
        characters.forEach(({
            image,
            name,
            species,
            created,
            id,
        }: CharacterType, index: number) => {
            const row = index % 2 === 0;
            const template = CharacterTemplate({
                row,
                id,
                image,
                name,
                species,
                created,
            });
            builder += template;
        });

        if (this.catalogueContainer !== null) {
            this.catalogueContainer.innerHTML = builder;
        }
    }

    private renderLocations({ locations }: any) {
        let builder = "";
        locations.forEach(({
            name,
            type,
            dimension,
            created,
            id,
        }: LocationType, index: number) => {
            const row = index % 2 === 0;
            const template = LocationTemplate({
                row,
                id,
                name,
                type,
                dimension,
                created,
            });
            builder += template;
        });

        if (this.catalogueContainer !== null) {
            this.catalogueContainer.innerHTML = builder;
        }
    }

    private renderEpisodes({ episodes }: any) {
        let builder = "";
        episodes.forEach(({
            name,
            episode,
            air_date,
            id,
        }: EpisodeType, index: number) => {
            const row = index % 2 === 0;
            const template = EpisodeTemplate({
                row,
                id,
                episode,
                name,
                air_date,
            });
            builder += template;
        });

        if (this.catalogueContainer !== null) {
            this.catalogueContainer.innerHTML = builder;
        }
    }
}

new Archive().load();
