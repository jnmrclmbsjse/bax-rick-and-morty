import axios from 'axios';
import Base from '@scripts/base';

class Episode extends Base {
    load() {
        if (this.subContainer) {
            this.subContainer.innerHTML = "Loading all characters, please wait...";
        }
        this.loadMain();
        this.pageLoader.open();
    }

    async loadMain() {
        this.pageLoader.open();
        let { data } = await axios.get(`/api/episode/${this.id}`);
        this.entity = data.episode;

        const template = await this.generateEpisodeTemplate();

        if (this.mainContainer !== null) {
            this.mainContainer.innerHTML = template;
        }
        setTimeout(() => {
            this.pageLoader.close();
            this.initializePaginator();
            this.loadSub();
        }, 1000);
    }

    async loadSub() {
        let characters: string[] = [];
        if (this.entity) {
            Object.values(this.entity.characters).forEach((character: any) => {
                characters.push(character.id);
            });
        }

        let ids = characters.join(',');
        let { data } = await axios.get(`/api/character/${ids}`);
        const template = this.generateCharacterTemplate(true, data);

        if (this.subContainer !== null) {
            this.subContainer.innerHTML = template;
        }
        setTimeout(() => {
            this.pageLoader.close()
        }, 1000);
    }

}

new Episode('episode', 'characters', 'episodeTextInput').load();
