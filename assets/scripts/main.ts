class Main {
    load() {
        this.initializeBulma();
    }

    private initializeBulma() {
        // Initialize dropdown toggle
        const dropdown: Element | null = document.querySelector('.dropdown');
        if (dropdown) {
            dropdown.addEventListener('click', (event) => {
                event.stopPropagation();
                dropdown.classList.toggle('is-active');
            });
        }
    }
}

new Main().load();
