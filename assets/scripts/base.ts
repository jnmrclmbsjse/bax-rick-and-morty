import PageLoader from "@scripts/utils/pageloader";
import { CharacterType, EpisodeType, LocationType } from '@scripts/utils/types';
import CharacterTemplate from '@templates/handlebars/character.handlebars';
import CharactersTemplate from '@templates/handlebars/characters.handlebars';
import EpisodesTemplate from '@templates/handlebars/episodes.handlebars';
import EpisodeTemplate from '@templates/handlebars/episode.handlebars';
import LocationTemplate from '@templates/handlebars/location.handlebars';

abstract class Base {
    mainContainer: HTMLElement | null;
    subContainer: HTMLElement | null;
    pageLoader: PageLoader;
    id: number;
    entity: any;

    constructor(main: string, sub: string, input: string) {
        this.mainContainer = document.getElementById(main);
        this.subContainer = document.getElementById(sub);
        const textInput: any = document.getElementById(input);
        this.id = textInput.value;
        this.entity = {};
        this.pageLoader = new PageLoader();
    }

    abstract load(): void;
    abstract loadMain(): void;
    abstract loadSub(): void;

    initializePaginator() {
        const previous = document.getElementById("previous");
        const next = document.getElementById("next");

        previous?.addEventListener('click', (event) => {
            if (this.id > 1) {
                this.id--;
                const title = document.title;
                let url = window.location.pathname;
                url = url.replace(/\/[^\/]*$/, `/${this.id.toString()}`);
                window.history.replaceState(null, title, url);
                this.load();
            }
        });

        next?.addEventListener('click', (event) => {
            if (this.id) {
                this.id++;
                const title = document.title;
                let url = window.location.pathname;
                url = url.replace(/\/[^\/]*$/, `/${this.id.toString()}`);
                window.history.replaceState(null, title, url);

                this.load();
            }
        });
    }

    generateCharacterTemplate(isMultiple: boolean = false, data?: any) {
        if (!isMultiple) {
            let {
                name,
                image,
                status,
                species,
                type,
                gender,
                origin,
                location
            }: CharacterType = this.entity;

            const template = CharacterTemplate({
                name,
                image,
                status,
                species,
                type,
                gender,
                origin,
                location
            });

            return template;
        }

        let builder = "";

        if (!data.characters) {
            const {
                image,
                name,
                species,
                created,
                id,
            } = data.character;
            builder = CharactersTemplate({
                row: false,
                id,
                image,
                name,
                species,
                created,
            });
        } else {
            data.characters.forEach(({
                image,
                name,
                species,
                created,
                id,
            }: CharacterType, index: number) => {
                const row = index % 2 === 0;
                const template = CharactersTemplate({
                    row,
                    id,
                    image,
                    name,
                    species,
                    created,
                });
                builder += template;
            });
        }

        return builder;
    }

    generateEpisodeTemplate(isMultiple: boolean = false, data?: any) {
        if (!isMultiple) {
            let {
                name,
                episode,
                air_date,
                created,
            }: EpisodeType = this.entity;

            const template = EpisodeTemplate({
                name,
                episode,
                air_date,
                created,
            });

            return template;
        }

        let builder = "";

        if (!data.episodes) {
            const {
                name,
                episode,
                air_date,
                id,
            } = data.episode;
            builder = EpisodesTemplate({
                row: false,
                id,
                episode,
                name,
                air_date,
            });
        } else {
            data.episodes.forEach(({
                name,
                episode,
                air_date,
                id,
            }: EpisodeType, index: number) => {
                const row = index % 2 === 0;
                const template = EpisodesTemplate({
                    row,
                    id,
                    episode,
                    name,
                    air_date,
                });
                builder += template;
            });
        }

        return builder;
    }

    generateLocationTemplate() {
        let {
            name,
            type,
            dimension,
            created,
        }: LocationType = this.entity;

        const template = LocationTemplate({
            name,
            type,
            dimension,
            created,
        });

        return template;
    }
}

export default Base;